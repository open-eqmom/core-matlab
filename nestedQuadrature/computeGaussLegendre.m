function [w,l] = computeGaussLegendre(Q)
% Method computing the parameters of a Gauss quadrature rules suited to
% approximate unweighted integrals over the support [-1; 1].
%
% int_{-1}^{1} f(x) dx ~= w * f(l)
%
% >> [w,l] = computeGaussLegendre(Q)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>1]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=1
  error('Wrong order of quadrature.')
end

% == COMPUTING QUADRATURE ==
J = zeros(Q,Q);
for i=1:(Q-1)
  J(i,i+1) = i/sqrt(4*i*i-1);
  J(i+1,i) = J(i,i+1);  
end
[V,l] = eig(J,'vector');
w = 2*V(1,:).^2;
end