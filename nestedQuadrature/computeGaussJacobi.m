function [w,l] = computeGaussJacobi(Q,a,b)
% Method computing the parameters of a Gauss quadrature rule suited to
% approximate integrals weighted by (1-x)^a*(1+x)^b over the support [-1, 1].
%
% int_{-1}^{1} f(x) (1-x)^a * (1+x)^b dx ~= w * f(l)
%
% >> [w,l] = computeGaussJacobi(Q,a,b)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>1]
%      + a     : Laguerre parameter               (1*1)[double,>-1]
%      + b     : Laguerre parameter               (1*1)[double,>-1]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=1
  error('Wrong order of quadrature.')
end
if numel(a)~=1 || ~isreal(a) || ~isfinite(a) || isnan(a) || a<=-1
  error('Invalid Jacobi parameter, a>-1 required');
end
if numel(b)~=1 || ~isreal(b) || ~isfinite(b) || isnan(b) || b<=-1
  error('Invalid Jacobi parameter, b>-1 required');
end

% == COMPUTING QUADRATURE ==
J = zeros(Q);
J(1,1) = (b*b-a*a)/((a+b)*(a+b+2));
for i=1:Q-1
  gam = 2*i+a+b;
  J(i+1,i+1) = (b*b-a*a)/(gam*(gam+2));
  J(i,i+1)   = sqrt(4*i*(i+a)*(i+b)*(i+a+b)/...
    ((gam)*(gam)*(gam*gam-1)));
  J(i+1,i)   = J(i,i+1);
end

[V,l] = eig(J,'vector');
w = 2^(a+b+1)*beta(a+1,b+1)*V(1,:).^2;
end