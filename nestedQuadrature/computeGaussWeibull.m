function [w,l] = computeGaussWeibull(Q,s)
% Method computing the parameters of a Gauss quadrature rules suited to
% approximate integrals weighted by the unshifted LogN distribution 
% over the support (0, +\inf).
%
% int_{0}^{+Inf} f(x) e^(-log(x)^2/(2*s^2))/(x*s*sqrt(2*pi)) dx ~= w * f(l)
%
% >> [w,l] = computeGaussWigert(Q,s)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>1]
%      + s     : shape parameter                  (1*1)[double,>0]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=1
  error('Wrong order of quadrature.')
end
if numel(s)~=1 || ~isreal(s) || ~isfinite(s) || isnan(s) || s<=0
  error('Invalid shape parameter, s>0 required');
end

% == COMPUTING QUADRATURE ==
[A,B] = chebyshev(gamma(1+s*(0:(2*Q-1))'));

J = zeros(Q);
J(1,1) = A(1);
for i=2:Q
   J(i,i) = A(i);
   J(i,i-1) = sqrt(B(i-1));
   J(i-1,i) = J(i,i-1);
end

[V,l] = eig(J,'vector');
w = V(1,:).^2;

end