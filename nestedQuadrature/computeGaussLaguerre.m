function [w,l] = computeGaussLaguerre(Q,a)
% Method computing the parameters of a Gauss quadrature rule suited to
% approximate integrals weighted by x^a*e^(-x) over the support [0, +inf).
%
% int_{0}^{+Inf} f(x) x^a exp(-x) dx ~= w * f(l)
%
% >> [w,l] = computeGaussLaguerre(Q,a)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>1]
%      + a     : Laguerre parameter               (1*1)[double,>-1]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=1
  error('Wrong order of quadrature.')
end
if numel(a)~=1 || ~isreal(a) || ~isfinite(a) || isnan(a) || a<=-1
  error('Invalid Laguerre parameter, a>-1 required');
end

% == COMPUTING QUADRATURE ==
J = zeros(Q);
J(1,1) = 1+a;
for i=1:Q-1
  J(i+1,i+1) = 2+J(i,i);
  J(i,i+1)   = sqrt(i*(i+a));
  J(i+1,i)   = J(i,i+1);
end

[V,l] = eig(J,'vector');
w = gamma(1+a)*V(1,:).^2;
end