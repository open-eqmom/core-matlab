function [w,l] = computeGaussHermite(Q)
% Method computing the parameters of a Gauss quadrature rules suited to
% approximate integrals weighted by e^(-x^2) over the support (-inf, +\inf).
%
% int_{-Inf}^{+Inf} f(x) e^(-x^2) dx ~= w * f(l)
%
% >> [w,l] = computeGaussHermite(Q)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>1]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=1
  error('Wrong order of quadrature.')
end

% == COMPUTING QUADRATURE ==
J = zeros(Q);
for i=1:Q-1
  J(i,i+1) = sqrt(.5*i);
  J(i+1,i) = J(i,i+1);
end
[V,l] = eig(J,'vector');
w = sqrt(pi)*V(1,:).^2;
end