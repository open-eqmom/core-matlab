function [w,l] = computeGaussLaplace(Q)
% Method computing the parameters of a Gauss quadrature rule suited to
% approximate integrals over the Laplace KDF.
%
% int_{-Inf}^{+Inf} f(x) e^(-abs(x))/2 dx ~= w * f(l)
%
% >> [w,l] = computeGaussLaplace(Q)
%   *Inputs :
%      + Q     : Quadrature order                 (1*1)[integer,>2]
%   *Outputs :
%      + w     : Row vector of weights            (1*Q)[double]
%      + l     : Column vector of nodes           (Q*1)[double]

% == CHECKING INPUTS ==
if numel(Q)~=1 || Q~=uint16(Q) || Q<=2
  error('Wrong order of quadrature.')
end

% == COMPUTING QUADRATURE THROUGH CHEBYSHEV ALGORITHM ==
S = zeros(Q-1,Q-1);
B = zeros(Q-1,1);

S(1,1) = 1;
for i=2:Q-1
  S(i,1) = S(i-1,1)*2*i*(2*i-1);
end
B(1,1) = 2;
for i=1:Q-2
  S(i,2) = S(i+1,1)-2*S(i,1);
end
B(2,1) = 10;
for j=3:Q-1
  for i=1:Q-j
    S(i,j) = S(i+1,j-1)-B(j-1,1)*S(i+1,j-2);
  end
  B(j,1) = S(1,j)/S(1,j-1);
end

J = diag(sqrt(B),-1);
J = J+J';

[V,l] = eig(J,'vector');
w = V(1,:).^2;

end