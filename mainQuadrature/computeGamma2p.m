function [W,L,sigma,status] = computeGamma2p(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% multi-Gamma reconstruction;
% Takes a vector of N moments as input and formulate a P nodes extended
% quadrature with N=2P+1 and P an integer.
%
% >> [W,L,sigma,status] = computeGamma2p(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (N*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Column vector of KDF weights     (P*1)[double,>=0]
%      + L     : Row vector of KDF locations      (1*P)[double,>=0]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]
%
% Explanations about the code structure and variables are given at the end
% of this file.

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);
nEvalMax = 100;

% == CHECKING INPUTS ==
N = numel(M);
if opts(1)
  if mod(N,2)==0  || size(M,2)~=1 || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1)=true; W = NaN; L = NaN; sigma = NaN; return;
  end
end

% == VARIABLE INITIALISATION ==
P = .5*(N-1);
A = zeros(P-1,4);
B = zeros(P,4);
S = zeros(N,P+1); S(1,1)=1;
T = ones(N-2,2);
Z = zeros(2*P-1,4);

p = [1 2 3 4 1 4 0];
s = [0 NaN NaN NaN];

% == PRECOMPUTATIONS ==
% -- Checking m0>0 --
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = nan(P,1); L = nan(1,P); sigma = NaN; return;
elseif M(1) == 0
  % -- Null distribution --
  status(2)=true; status(3)=true;
  W = zeros(P,1); L = .5+zeros(1,P); sigma = 0;
  if opts(2) % Checking that all moments are null
    status(10)=true;
    status(9)=~any(M~=0);
  end
  return;
end

% -- Reduced moments --
M0 = M(1);
iM0 = 1/M0;
MB = zeros(N-1,1);
for i=2:N
  MB(i-1) = M(i)*iM0;
end

% == SEARCH INITIALIZATION ==
% -- Chebyshev algorithm with s = 0 --
for i=1:N-1
  S(i+1,1) = MB(i);
end
for j=0:(N-3); S(j+2,2) = S(j+3,1)-MB(1)*S(j+2,1); end
B(1,1) = S(2,2)/S(1,1);
Z(1,1) = B(1,1)/MB(1);
A(1,1) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
Z(2,1) = A(1,1) - Z(1,1);
for i=3:P
  for j = 0:(N+1-2*i)
    S(i+j,i) = S(i+j+1,i-1)-A(i-2,1)*S(i+j,i-1)-B(i-2,1)*S(i+j,i-2);
  end
  B(i-1,1) = S(i,i)/S(i-1,i-1);
  Z(2*i-3,1) = B(i-1,1)/Z(2*i-4,1);
  A(i-1,1) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  Z(2*i-2,1) = A(i-1,1)-Z(2*i-3,1);
end
S(P+1,P+1) = S(P+2,P)-A(P-1,1)*S(P+1,P)-B(P-1,1)*S(P+1,P-1);
B(P,1) = S(P+1,P+1)/S(P,P);
Z(2*P-1,1) = B(P,1)/Z(2*P-2,1);

% -- Check realizability of raw moments --
for i=1:2*P-1
  if Z(i,1)<-relTol
    % - Unrealisable moments -
    status(5) = true;
    W = nan(P,1);
    L = nan(1,P);
    sigma = NaN;
    return;
  elseif Z(i,1)<relTol
    % - Degenerated moments -
    Z(i,1) = 0; %Purify value
    status(2)=true;
    break
  end
end

% -- Chebyshev algorithm with s = smax --
if status==0
  s(4) = MB(2)/MB(1)-MB(1);
  
  % - Degenerated moments -
  k1=2; k2=1; f=1;
  for i=1:N-2
    k3=k1; k1=k2; k2=k3;
    f=-f*s(4);
    S(i+2,1)=S(i+2,1)+f*MB(1);
    for j=2:(N-1-i)
      T(j,k1) = T(j-1,k1)+j*T(j,k2);
      S(i+j+1,1)=S(i+j+1,1)+f*T(j,k1)*MB(j);
    end
  end
  % - End degenerated moments -
  
  for i=0:(N-3); S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1); end
  B(1,4) = S(2,2)/S(1,1);
  Z(1,4) = B(1,4)/MB(1);
  A(1,4) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  Z(2,4) = A(1,4) - Z(1,4);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,4)*S(i+j,i-1)-B(i-2,4)*S(i+j,i-2);
    end
    B(i-1,4) = S(i,i)/S(i-1,i-1);
    Z(2*i-3,4) = B(i-1,4)/Z(2*i-4,4);
    A(i-1,4) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
    Z(2*i-2,4) = A(i-1,4)-Z(2*i-3,4);
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,4)*S(P+1,P)-B(P-1,4)*S(P+1,P-1);
  B(P,4) = S(P+1,P+1)/S(P,P);
  Z(2*P-1,4) = B(P,4)/Z(2*P-2,4);
end

% == INTERMEDIATE SEARCH ==
Zref = Z(:,1)*relTol;
sref = s(4)*relTol;
nEval = 2;

k=1;
while status==0
  if Z(k,p(4))>0, k=k+1; if k==2*P-1, break, end, continue, end
  
  % -- Reset pointers to next-bound candidates --
  p(5) = 1;
  p(6) = 4;
  
  % == First step of Ridder's method ==
  % -- Compute sigma value --
  s(p(2)) = .5*(s(p(1))+s(p(4)));
  
  % -- Compute degenerated moments --
  for i=3:N, S(i,1)=MB(i-1); end
  f=1; T(:)=1;
  for i=1:N-2
    k3=k1; k1=k2; k2=k3;
    f=-f*s(p(2));
    S(i+2,1)=S(i+2,1)+f*MB(1);
    for j=2:(N-1-i)
      T(j,k1) = T(j-1,k1)+j*T(j,k2);
      S(i+j+1,1)=S(i+j+1,1)+f*T(j,k1)*MB(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(2)) = S(2,2)/S(1,1);
  Z(1,p(2)) = B(1,p(2))/MB(1);
  A(1,p(2)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  Z(2,p(2)) = A(1,p(2)) - Z(1,p(2));
  
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(2))*S(i+j,i-1)-B(i-2,p(2))*S(i+j,i-2);
    end
    B(i-1,p(2)) = S(i,i)/S(i-1,i-1);
    Z(2*i-3,p(2)) = B(i-1,p(2))/Z(2*i-4,p(2));
    A(i-1,p(2)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
    Z(2*i-2,p(2)) = A(i-1,p(2))-Z(2*i-3,p(2));
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(2))*S(P+1,P)-B(P-1,p(2))*S(P+1,P-1);
  B(P,p(2)) = S(P+1,P+1)/S(P,P);
  Z(2*P-1,p(2)) = B(P,p(2))/Z(2*P-2,p(2));
  
  % -- Arbitrate about the value s(p(2)) --
  t1 = Z(k,p(2))>0;
  t2 = false;
  if t1
    for i=k+1:2*P-1, if Z(i,p(2))<0, t2=true; break; end, end
  end
  
  if t2
    % p(2) becomes the new right bound and we jump to the next iteration
    p(7) = p(4); p(4) = p(2); p(2) = p(7); continue;
  elseif t1
    % p(2) becomes candidate to be the next left bound
    p(5) = 2;
  else
    % p(2) becomes candidate to be the next right bound
    p(6) = 2;
  end
  
  % == Second step of Ridder's method ==
  % -- Compute sigma value --
  s(p(3)) = s(p(2)) + (s(p(2))-s(p(1)))*Z(k,p(2))/...
    sqrt(Z(k,p(2))*Z(k,p(2))-Z(k,p(1))*Z(k,p(4)));
  
  % -- Compute degenerated moments --
  for i=3:N, S(i,1)=MB(i-1); end
  f=1; T(:)=1;
  for i=1:N-2
    k3=k1; k1=k2; k2=k3;
    f=-f*s(p(3));
    S(i+2,1)=S(i+2,1)+f*MB(1);
    for j=2:(N-1-i)
      T(j,k1) = T(j-1,k1)+j*T(j,k2);
      S(i+j+1,1)=S(i+j+1,1)+f*T(j,k1)*MB(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(3)) = S(2,2)/S(1,1);
  Z(1,p(3)) = B(1,p(3))/MB(1);
  A(1,p(3)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  Z(2,p(3)) = A(1,p(3)) - Z(1,p(3));
  
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(3))*S(i+j,i-1)-B(i-2,p(3))*S(i+j,i-2);
    end
    B(i-1,p(3)) = S(i,i)/S(i-1,i-1);
    Z(2*i-3,p(3)) = B(i-1,p(3))/Z(2*i-4,p(3));
    A(i-1,p(3)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
    Z(2*i-2,p(3)) = A(i-1,p(3))-Z(2*i-3,p(3));
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(3))*S(P+1,P)-B(P-1,p(3))*S(P+1,P-1);
  B(P,p(3)) = S(P+1,P+1)/S(P,P);
  Z(2*P-1,p(3)) = B(P,p(3))/Z(2*P-2,p(3));
  
  % -- Arbitrate about the value s(p(3)) --
  t1 = Z(k,p(3))>0;
  t2 = false;
  if t1
    for i=k+1:2*P-1, if Z(i,p(3))<0, t2=true; break; end, end
  end
  
  if ~t2 && t1 && s(p(3))>s(p(p(5)))
    % s(p(3)) becomes candidate to be the next left bound
    p(5) = 3;
  end
  if ~t1 && s(p(3))<s(p(p(6)))
    % s(p(3)) becomes candidate to be the next right bound
    p(6) = 3;
  end
  
  % == Apply new bounds ==
  p(7) = p(1); p(1) = p(p(5)); p(p(5)) = p(7);
  p(7) = p(4); p(4) = p(p(6)); p(p(6)) = p(7);
  
  % == Check partial convergence ==
  if s(p(4))-s(p(1))< sref, status(8)=true; end
  if nEval > nEvalMax, status(6)=true; end
end


% == FINAL SEARCH ==
while status == 0
  % -- Reset pointers to next-bound candidates --
  p(5) = 1;
  p(6) = 4;
  
  % == First step of Ridder's method ==
  % -- Compute sigma value --
  s(p(2)) = .5*(s(p(1))+s(p(4)));
  
  % -- Compute degenerated moments --
  for i=3:N, S(i,1)=MB(i-1); end
  f=1; T(:)=1;
  for i=1:N-2
    k3=k1; k1=k2; k2=k3;
    f=-f*s(p(2));
    S(i+2,1)=S(i+2,1)+f*MB(1);
    for j=2:(N-1-i)
      T(j,k1) = T(j-1,k1)+j*T(j,k2);
      S(i+j+1,1)=S(i+j+1,1)+f*T(j,k1)*MB(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(2)) = S(2,2)/S(1,1);
  Z(1,p(2)) = B(1,p(2))/MB(1);
  A(1,p(2)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  Z(2,p(2)) = A(1,p(2)) - Z(1,p(2));
  
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(2))*S(i+j,i-1)-B(i-2,p(2))*S(i+j,i-2);
    end
    B(i-1,p(2)) = S(i,i)/S(i-1,i-1);
    Z(2*i-3,p(2)) = B(i-1,p(2))/Z(2*i-4,p(2));
    A(i-1,p(2)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
    Z(2*i-2,p(2)) = A(i-1,p(2))-Z(2*i-3,p(2));
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(2))*S(P+1,P)-B(P-1,p(2))*S(P+1,P-1);
  B(P,p(2)) = S(P+1,P+1)/S(P,P);
  Z(2*P-1,p(2)) = B(P,p(2))/Z(2*P-2,p(2));
  
  % -- Arbitrate about the value s(p(2)) --
  if Z(2*P-1,p(2))>=0
    % p(2) becomes candidate to be the next left bound
    p(5) = 2;
  else
    % p(2) becomes candidate to be the next right bound
    p(6) = 2;
  end
  
  % == Second step of Ridder's method ==
  % -- Compute sigma value --
  s(p(3)) = s(p(2)) + (s(p(2))-s(p(1)))*Z(2*P-1,p(2))/...
    sqrt(Z(2*P-1,p(2))*Z(2*P-1,p(2))-Z(2*P-1,p(1))*Z(2*P-1,p(4)));
  
  % -- Compute degenerated moments --
  for i=3:N, S(i,1)=MB(i-1); end
  f=1; T(:)=1;
  for i=1:N-2
    k3=k1; k1=k2; k2=k3;
    f=-f*s(p(3));
    S(i+2,1)=S(i+2,1)+f*MB(1);
    for j=2:(N-1-i)
      T(j,k1) = T(j-1,k1)+j*T(j,k2);
      S(i+j+1,1)=S(i+j+1,1)+f*T(j,k1)*MB(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(3)) = S(2,2)/S(1,1);
  Z(1,p(3)) = B(1,p(3))/MB(1);
  A(1,p(3)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  Z(2,p(3)) = A(1,p(3)) - Z(1,p(3));
  
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(3))*S(i+j,i-1)-B(i-2,p(3))*S(i+j,i-2);
    end
    B(i-1,p(3)) = S(i,i)/S(i-1,i-1);
    Z(2*i-3,p(3)) = B(i-1,p(3))/Z(2*i-4,p(3));
    A(i-1,p(3)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
    Z(2*i-2,p(3)) = A(i-1,p(3))-Z(2*i-3,p(3));
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(3))*S(P+1,P)-B(P-1,p(3))*S(P+1,P-1);
  B(P,p(3)) = S(P+1,P+1)/S(P,P);
  Z(2*P-1,p(3)) = B(P,p(3))/Z(2*P-2,p(3));
  
  % -- Arbitrate about the value s(p(3)) --
  if Z(2*P-1,p(3))>=0 && s(p(3))>s(p(p(5)))
    p(5) = 3;
  elseif Z(2*P-1,p(3))<0 && s(p(3))<s(p(p(6)))
    p(6) = 3;
  end
  
  % == Apply new bounds ==
  p(7) = p(1); p(1) = p(p(5)); p(p(5)) = p(7);
  p(7) = p(4); p(4) = p(p(6)); p(p(6)) = p(7);
  
  % == Check convergence ==
  if Z(2*P-1,p(1))<Zref(2*P-1)
    Z(2*P-1,p(1))=0; status(7)=true; status(8)=true; status(9)=true;
  end
  if s(p(4))-s(p(1))< sref, status(8)=true; end
  if nEval > nEvalMax, status(6)=true; end
end


% == QUADRATURE COMPUTATION ==
% -- Detect number of nodes --
n=P;
for i=2:2*P
  if Z(i-1,p(1))<=Zref(i-1)
    if mod(i,2)==1
      n=.5*(i+1);
      A(n,p(1)) = Z(i-1,p(1)); % Purifying values
      status(4)=true;
    else
      n=.5*i;
      B(n,p(1)) = 0; % Purifying values
    end
    break;
  end
end

% -- Constructing Jacobi matrix --
J = zeros(n,n);
J(1,1) = MB(1);
for i=2:n
  J(i,i) = A(i-1,p(1));
  J(i,i-1) = sqrt(B(i-1,p(1)));
  J(i-1,i) = J(i,i-1);
end

% -- Computing weights and nodes --
sigma = s(p(1));
if n==P
  [V,L] = eig(J,'vector');
  L=L';
  W = M0*V(1,:)'.^2;
else
  [V,l] = eig(J,'vector');
  w = M0*V(1,:).^2;
  L = [l' .5+zeros(1,P-n)];
  W = [w'; zeros(P-n,1)];
  status(3)=true;
end

% -- Purifying first node if null value expected --
if status(4)
  L(1) = 0;
end

% -- Checking moments preservation --
if opts(2) && ~status(10)
  % Compute final star moments
  Lp = ones(1,P);
  mStar = zeros(N,1); mStar(1) = M0;
  for i=2:N
    Lp = Lp.*L;
    mStar(i) = Lp*W;
  end
  
  % Compute raw moments if sigma>0
  mRaw = mStar;
  if ~status(2)
    T = ones(2*N-4,1);
    k1 = 0; k2 = N-2; f = 1;
    
    for i=1:N-2
      k3 = k1; k1 = k2; k2 = k3;
      f = f*sigma;
      T(k1+1,1) = i*T(k2+1,1);
      mRaw(i+2,:) = mRaw(i+2,:) + T(k1+1,1)*f.*mRaw(2,:);
      for j=2:(N-1-i)
        T(k1+j,1) = T(k1+j-1,1) + (j+i-1)*T(k2+j,1);
        mRaw(i+1+j,:) = mRaw(i+1+j,:) + T(k1+j,1)*f.*mStar(j+1,:);
      end
    end
  end
  
  % Measure relative error between mRaw and M
  err = 2*norm((mRaw-M)./(mRaw+M));
  
  % Set status bits
  status(9)=err<relTol;
  status(10)=true;
end

end
% == CODE EXPLANATION ==
% -- Structure --
%  This codes solves for the value of sigma that yields:
%    zeta_k(\sigma)>0, k\in{1,...,2P-1}
%    zeta_2P(\sigma)=0
%
%  The call to external functions is kept at the minimum to make the code
%  faster, by minimising the number of memory allocations and releases.
%  The only external function called is ``eig'' at the end of the
%  computation (with the exception of basic mathematical operations).
%
%  The search of the solution if splitted in three steps:
%   i.   Evaluation of zeta_k for s = 0 and s = s_max.
%   ii.  Search of interval [s_l, s_r] in which zeta_k(s)>0
%        k\in{1,...,2P-1}, zeta_2P(s_l)>0 and zeta_2P(s_r)<0.
%   iii. Search of the value s such that zeta_2P(s)=0.
%
%  The steps ii. and iii. are implementations of Ridder's method.
%
%  In step ii., two logical flags are associated to each tested s value.
%  These allow to stop the computations of the Chebyshev algorithm as soon
%  as enough information is known about the tested s value to detect
%  whether it is higher or lower than the searched root.
%
%  For each tested value of s, we first compute the associated
%  degenerated moments. These are placed in the first column of the matrix
%  ``S'' and are computed through a simple for-loop instead of performing the
%  whole matrix-vector product described in the documentation.
%
%  The variable ``p'' acts as a pointer. More precisely, the values p(1:4)
%  contains the column numbers in which are holds the informations about
%  tested s values:
%   * columns p(1) of A, B, Z and s hold information about the left bound of
%   the search interval
%   * columns p(4) of A, B, Z and s hold information about the right bound
%   * columns p(2) and p(3) of A, B, Z and s are used to store informations
%   of the two s values tested by Ridder's algorithm.
%
%  At the end of each Ridder's iteration, Z(:,p(1)) contains only positive
%  values, and Z(:,p(4)) contains at least one negative value.
%
% -- Variables --
% * A : matrix whose columns hold the a_k coefficients, k\in{2,...,P-1}, for
%       multiple s values.
% * B : matrix whose columns hold the b_k coefficients, k\in{1,...,P}, for
%       multiple s values.
% * Z : matrix whose columns hold the zeta_k values, k\in{2,...,2P}, for
%       multiple s values
% * s : row vector holding the tested sigma values.
% * S : matrix used to compute the Chebyshev algorithm
% * MB: vector of reduced moments, defined by m_k/m_0.