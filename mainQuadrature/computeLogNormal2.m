function [W,L,sigma,status] = computeLogNormal2(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% bi-LogN reconstruction;
% Takes a vector of 5 moments as input and formulates a 2 nodes extended
% quadrature.
%
% >> [W,L,sigma,status] = computeLogNormal2(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (5*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Column vector of KDF weights     (2*1)[double,>=0]
%      + L     : Row vector of KDF locations      (1*2)[double,>=0]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);

% == CHECKING INPUTS ==
if opts(1)
  if ~isequal(size(M),[5 1]) || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  end
end

% == INPUT PREPROCESSING ==
% -- Checking m0>0 --
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
elseif M(1) == 0
  % -- Null distribution --
  status(2)=true; status(3)=true;
  W = [0; 0]; L = [.5 .5]; sigma = 0;
  if opts(2) % Checking that all moments are null
    status(10) = true;
    status(9) = ~any(M~=0);
  end
  return;
end

% -- Computing reduced moments --
iM0 = 1/M(1);
MB1 = M(2)*iM0;
MB2 = M(3)*iM0;
MB3 = M(4)*iM0;
MB4 = M(5)*iM0;

% -- Often used square values --
MB1S = MB1*MB1;
MB2S = MB2*MB2;

% == INITIAL REALIZABILITY CHECK ==
% -- Computation of Hankel determinants --
H = [MB1;
  MB2-MB1S;
  MB1*MB3-MB2S;
  0];
H(4) = MB4*H(2) - MB3*(MB3 - MB2*MB1) + MB2*(MB3*MB1-MB2S);


% -- Test of Hankel determinants values --
for i=1:4
  if H(i)<-relTol
    % -- Unrealisable moments --
    status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  elseif H(i)<relTol
    % -- Denegerated moments --
    status(2)=true;
    sigma = 0;
    break;
  end
end

% == COMPUTING QUADRATURE ==
if status == 0
  b1 = MB4*MB1S+MB3*MB3;
  b2 = 1/(MB4*MB2);
  
  c0 = b1*b2;
  c0s = c0*c0;
  c1 = -.375*c0s;
  c2 = 2*MB3*MB1/MB4- .125*c0s*c0;
  c3 = 6 * MB3*MB2*MB1*b1-12*MB4*MB2S*MB2S;
  c4 = -27*MB2*MB2S*(MB4*MB1S-MB3*MB3)^2;
  c5 = (4*c3*c3*c3-c4*c4)/27;
  
  if c5>0
    c3sr = sqrt(c3);
    d1 = sqrt(2/3*(c3sr*b2*cos(acos(.5*c4/(c3*c3sr))/3)-c1));
    sr = .25*c0+.5*(d1+sqrt(-d1*d1-2*(c1+c2/d1)));
  else
    if abs(4*c3*c3*c3)<relTol*abs(c4*c4)
      d2 = c4;
    else
      d2 = .5*(c4+sqrt(-27*c5));
    end
    if d2>0
      d2 = d2^(1/3);
    else
      d2 = -(-d2)^(1/3);
    end
    d1 = sqrt(((d2+c3/d2)*b2-2*c1)/3);
    v = 2*(c2/d1-c1) -d1*d1;
    if v>=0
      sr = .25*c0 + .5*(sqrt(v)-d1);
    else
      sr = .25*c0 + .5*(sqrt(-2*(c2/d1+c1) -d1*d1)+d1);
    end
  end
  z=sqrt(sr);
  
  sigma = sqrt(-log(sr));
  status(8)=true;
else
  sr = 1;
  z = 1;
end

e1 = sr*(MB2*sr-MB1S);
e2 = z*sr*(sr*(MB3*sr*sr-3*MB2*MB1)+2*MB1*MB1S);
if abs(e2) < relTol || (4*e1*e1*e1<relTol*e2*e2)
  W = [M(1); 0];
  L = [MB1 .5];
  status(3)=true;
else
  e3 = e2/sqrt(e2*e2+4*e1*e1*e1);
  if (1+e3)*MB1S*sr+e1*(e3-1)>relTol
    W = .5*M(1)*[1+e3; 1-e3];
    L = MB1*z+[-sqrt(e1*(1-e3)/(1+e3)) sqrt(e1*(1+e3)/(1-e3))];
    status(7)=false; status(9)=true; status(10)=true;
  else
    % == Compute quadrature with L(1)=0 ==
    sr = MB2S/(MB3*MB1);
    z = sqrt(sr);
    sigma = sqrt(-log(sr));
    L = [0 MB2*z*sr/MB1];
    W2 = MB1S/(MB2*sr);
    W = M(1)*[1-W2; W2];
    status(4)=true; status(7)=false; status(9)=false; status(10)=true;
  end
end
end