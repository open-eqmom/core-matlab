function option = eqmomset( varargin )
% Function defining a logical array that encodes options for EQMOM solvers.
%
% >> code = eqmomset( option1, value1, option2, value2, ... )
%   *Inputs :
%      + optionX : Name of option n�X             (1*?)[char]
%      + valueX  : Value of option n�X            (1*1)[logical]
%   *Outputs :
%      + option  : logical array for solvers      (1*8)[logical]
%
% - Available options -
% * checkInputs:            If true, EQMOM solvers check the format of inputs.
%                         Otherwise, format is not checked which increases
%                         speed at risk of wrong formatting and potential
%                         execution errors.
% * checkMomentPreserved:   If true, EQMOM solvers will perform firther tests
%                         to check whether all inputs moments were
%                         preserved.

% == Default option code ==
option = false(1,8);
option(1) = true;

% == Read used defined options ==
i=1;
while i<nargin
  optName = varargin{i};
  if ~ischar(optName)
    warning('Input %u is not a valid option name.',i);
    i = i+1;
    continue
  end
  optValue = varargin{i+1};
  if ~islogical(optValue) || ~isequal(size(optValue),[1 1])
    warning('Value for option %s is not valid.',optName);
  end
  switch lower(optName)
    case 'checkinputs'
      option(1) = optValue;
    case 'checkmomentpreserved'
      option(2) = optValue;
    otherwise
      warning('Input %u is not a defined option name.',i);
  end
  i = i+2;
end

end

