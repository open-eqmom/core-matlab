function [W,L,sigma,status] = computeWeibull1(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% single Weibull kernel.
%
% >> [W,L,sigma,status] = computeWeibull1(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (3*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Row vector of KDF weights        (1*1)[double]
%      + L     : Column vector of KDF locations   (1*1)[double,>0]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);
nEvalMax = 100;

% == CHECKING INPUTS ==
if opts(1)
  if ~isequal(size(M),[3 1]) || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1) = true; W = NaN; L = NaN; sigma = NaN; return;
  end
end

% == CHECKING M0 POSITIVITY ==
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = NaN; L = NaN; sigma = NaN; return;
elseif M(1)==0
  % -- Null distribution --
  W=0; L=.5; sigma=0;
  status(2) = true; status(3) = true; status(7) = true; status(8) = true;
  status(9) = true;
  if opts(2)
    status(10) = true;
    status(9) = ~any(M~=0);
  end
  return;
end

% == COMPUTING QUADRATURE ==
k = M(3)*M(1)/(M(2)*M(2));

if k<1-relTol
  W = NaN; L = NaN; sigma = NaN; status(5)=true; return
elseif k<1+relTol
  W = M(1); L = M(2)/M(1); sigma = 0;
  status(2) = true; status(7) = true; status(8) = true; status(9) = true;
  status(10) = true;
else
  % We look for the root of G(s) = k-Gamma((1+s)/(1-s))/(Gamma(1/(1-s)))^2
  % on s in [0, 1) with k=m2*m0/m1^2.
  % G has the properties G(0)>0 and G(1-eps)<0.
  
  rk = 1e-10*(k-1); %Value under which we consider G(s) null.
  
  % First round of dichotomic algorithm leads to the following initialisation
  if k>2
    l = .5;  vl = k-2;
    r = NaN; vr = NaN;
  else
    l =  0;   vl = k-1;
    r = .5;   vr = k-2;
  end
  
  % Identifying right bound if not already.
  % We use the property G(n/(n+1))=k-(2n)!/(n!)^2
  % by increasing continuously the value of n.
  i = 1; v = 2;
  while isnan(r)
    i = i+1;
    v = v*(4-2/i);
    if v>=k
      r = i/(i+1); vr = k-v;
    else
      l = i/(i+1); vl = k-v;
    end
  end
  
  i = 0;
  while i<nEvalMax
    i = i+1;
    c1 = .5*(r+l);
    d = 1/(1-c1);
    vc1 = k-gamma(d*(1+c1))/gamma(d)^2;
    
    c2 = c1 + (c1-l) * vc1 / sqrt(vc1*vc1 - vl*vr);
    d = 1/(1-c2);
    vc2 = k-gamma(d*(1+c2))/gamma(d)^2;
    
    if abs(vc2)<rk; break; end % End of iterations
    if vc2>0
      l = c2; vl = vc2;
      if vc1<0
        r = c1; vr = vc1;
      end
    else
      r = c2; vr = vc2;
      if vc2>0
        l = c1; vl = vc1;
      end
    end
  end
  
  status(7) = true; status(9) = true; status(10) = true;
  sigma = c2*d;
  W = M(1);
  L = M(2)/(M(1)*gamma(1+sigma));
end
end