function [W,L,sigma,status] = computeGamma1(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% single Gamma kernel.
%
% >> [W,L,sigma,status] = computeGamma1(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (3*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Row vector of KDF weights        (1*1)[double]
%      + L     : Column vector of KDF locations   (1*1)[double,>0]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-12;
status   = false(1,16);

% == CHECKING INPUTS ==
if opts(1)
  if ~isequal(size(M),[3 1]) || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1) = true; W = NaN; L = NaN; sigma = NaN; return;
  end
end

% == CHECKING M0 POSITIVITY ==
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = NaN; L = NaN; sigma = NaN; return;
elseif M(1)==0
  % -- Null distribution --
  W=0; L=.5; sigma=0;
  status(2) = true; status(3) = true; status(7) = true; status(8) = true;
  status(9) = true;
  if opts(2)
    status(10) = true;
    status(9) = ~any(M~=0);
  end
  return;
end

% == COMPUTING QUADRATURE ==
W = M(1);
L = M(2)/M(1);
sigma = M(3)/M(2)-M(2)/M(1);

if sigma<=0 && sigma>-relTol
  % == PURIFYING sigma ==
  sigma=0;
  status(2) = true; status(7) = true; status(8) = true; status(9) = true;
  status(10) = true;
elseif (L<0) || (isnan(sigma) || sigma<0 || isinf(sigma))
  status(5)=true; W = NaN; L = NaN; sigma = NaN; return;
else
  status(7) = true; status(8) = true; status(9) = true; status(10) = true;
end
end