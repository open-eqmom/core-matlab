function [W,L,sigma,status] = computeGauss2(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% bi-Gaussian reconstruction;
% Takes a vector of 5 moments as input and formulates a 2 nodes extended
% quadrature.
%
% >> [W,L,sigma,status] = computeGauss2(M)
%   *Inputs :
%      + M     : Vector of realisable moments     (5*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Column vector of KDF weights     (2*1)[double,>=0]
%      + L     : Row vector of KDF locations      (1*2)[double]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);

% == CHECKING INPUTS ==
if opts(1)
  if ~isequal(size(M),[5 1]) || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  end
end

% == INPUT PREPROCESSING ==
% -- Checking m0>0 --
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
elseif M(1) == 0
  % -- Null distribution --
  status(2)=true; status(3)=true;
  W = [0; 0]; L = [.5 .5]; sigma = 0;
  if opts(2) % Checking that all moments are null
    status(10) = true;
    status(9) = ~any(M~=0);
  end
  return;
end

% -- Computing reduced moments --
iM0 = 1/M(1);
MB1 = M(2)*iM0;
MB2 = M(3)*iM0;
MB3 = M(4)*iM0;
MB4 = M(5)*iM0;

% -- Often used square values --
MB1S = MB1*MB1;

% == INITIAL REALIZABILITY CHECK ==
% -- Computation of realisability criteria --
B = [M(1);
  MB2-MB1S;
  MB4*(MB2-MB1S)+2*MB1*MB2*MB3-MB2*MB2*MB2-MB3*MB3];

% -- Test of realisability criteria values --
for i=1:3
  if B(i)<-relTol
    % -- Unrealisable moments --
    status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  elseif B(i)<relTol
    % -- Degenerated moments --
    status(2)=true;
    sigma = 0;
    break;
  end
end

% == COMPUTING QUADRATURE ==
e2 = MB3-3*MB2*MB1+2*MB1S*MB1;
if status == 0
  c1 = .5*(MB4-4*MB3*MB1-3*MB2*MB2+12*MB2*MB1S-6*MB1S*MB1S);
  c2 = -.5*e2*e2;
  Delta = 4*c1*c1*c1+27*c2*c2;
  if abs(Delta)<relTol
    e1 = 0;
  elseif Delta>0
    d = (.5*(sqrt(27*Delta)-27*c2))^(1/3);
    e1 = d/3-c1/d;
  else
    d = sqrt(-c1/3);
    e1 = 2*d*cos(acos(1.5*c2/(d*c1))/3);
  end
  sigma = sqrt(MB2-MB1S-e1);
  status(7)=true; status(8)=true;
else
  e1 = MB2-MB1S;
end
e3 = e2/sqrt(e2*e2+4*e1*e1*e1);
status(8)=true; status(9)=true; status(10)=true;

if abs(e1)<relTol || (4*e1*e1*e1<relTol*e2*e2)
  W = [M(1); 0];
  L = [MB1 .5];
  status(3)=true;
else
  W = .5*M(1)*[1+e3; 1-e3];
  L = MB1+[-sqrt(e1*(1-e3)/(1+e3)) sqrt(e1*(1+e3)/(1-e3))];
end
end