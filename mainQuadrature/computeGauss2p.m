function [W,L,sigma,status] = computeGauss2p(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% multi-Gauss reconstruction;
% Takes a vector of N moments as input and formulate a P nodes extended
% quadrature with with N=2P+1 and P an integer.
%
% >> [W,L,sigma,status] = computeGauss2p(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (N*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Column vector of KDF weights     (P*1)[double,>=0]
%      + L     : Row vector of KDF locations      (1*P)[double]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]
%
% The status code can be translated into a structure of flags with
% eqmomstatus.
%
% Explanations about the code structure and variables are given at the end
% of this file.

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);
nEvalMax = 100;

% == CHECKING INPUTS ==
N = numel(M);

if opts(1)
  if mod(N,2)==0  || size(M,2)~=1 || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1)=true; W = NaN; L = NaN; sigma = NaN; return;
  end
end

% == VARIABLE INITIALISATION ==
P = .5*(N-1);
E = ones(N-2,1);
A = zeros(P-1,4);
B = zeros(P,4);
S = zeros(N,P+1); S(1,1)=1;

p = [1 2 3 4 1 4 0];
s2 = [0 NaN NaN NaN];

% == PRECOMPUTATIONS ==
% -- Checking m0>0 --
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = nan(P,1); L = nan(1,P); sigma = NaN; return;
elseif M(1) == 0
  % -- Null distribution --
  status(2)=true; status(3)=true;
  W = zeros(P,1); L = .5+zeros(1,P); sigma = 0;
  if opts(2) % Checking that all moments are null
    status(10)=true;
    status(9)=~any(M~=0);
  end
  return;
end

% -- Reduced moments --
M0 = M(1);
iM0 = 1/M0;
MB = zeros(N-1,1);
for i=2:N
  MB(i-1) = M(i)*iM0;
end

% == SEARCH INITIALIZATION ==
% -- Chebyshev algorithm with s2 = 0 --
for i=1:N-1
  S(i+1,1) = MB(i);
end
for j=0:(N-3); S(j+2,2) = S(j+3,1)-MB(1)*S(j+2,1); end
B(1,1) = S(2,2)/S(1,1);
A(1,1) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
for i=3:P
  for j = 0:(N+1-2*i)
    S(i+j,i) = S(i+j+1,i-1)-A(i-2,1)*S(i+j,i-1)-B(i-2,1)*S(i+j,i-2);
  end
  B(i-1,1) = S(i,i)/S(i-1,i-1);
  A(i-1,1) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
end
S(P+1,P+1) = S(P+2,P)-A(P-1,1)*S(P+1,P)-B(P-1,1)*S(P+1,P-1);
B(P,1) = S(P+1,P+1)/S(P,P);

% -- Check realizability of raw moments --
for i=1:P
  if B(i,1)<-relTol
    % - Unrealisable moments -
    status(5) = true;
    W = nan(P,1);
    L = nan(1,P);
    sigma = NaN;
    return;
  elseif B(i,1)<relTol
    % - Degenerated moments -
    B(i,1) = 0; %Purify value
    status(2)=true; break;
  end
end

% -- Chebyshev algorithm with s2 = s2max --
if status==0
  s2(4) = MB(2)-MB(1)*MB(1);
  
  % - Degenerated moments -
  for i=1:P
    E(1) = -E(2)*s2(4); S(1+2*i,1) = S(1+2*i,1)+E(1);
    for j=2:(N-2*i)
      E(j) = E(j-1)*(2*i+j-1)/(j-1);
      S(j+2*i,1) = S(j+2*i,1) + MB(j-1)*E(j);
    end
  end
  % - End degenerated moments -
  
  for i=0:(N-3); S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1); end
  B(1,4) = S(2,2)/S(1,1);
  A(1,4) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,4)*S(i+j,i-1)-B(i-2,4)*S(i+j,i-2);
    end
    B(i-1,4) = S(i,i)/S(i-1,i-1);
    A(i-1,4) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,4)*S(P+1,P)-B(P-1,4)*S(P+1,P-1);
  B(P,4) = S(P+1,P+1)/S(P,P);
end

% == INTERMEDIATE SEARCH ==
Bref = B(:,1)*relTol;
s2ref = s2(4)*relTol;
nEval = 2;

k=1;
while status==0
  if B(k,p(4))>0, k=k+1; if k==P, break, end, continue, end
  
  % -- Reset pointers to next-bound candidates --
  p(5) = 1;
  p(6) = 4;
  
  % == First step of Ridder's method ==
  % -- Compute sigma value --
  s2(p(2)) = .5*(s2(p(1))+s2(p(4)));
  
  % -- Compute degenerated moments --
  for i=1:N-1; S(i+1,1) = MB(i); end, E(2) = 1;
  for i=1:P
    E(1) = -E(2)*s2(p(2)); S(1+2*i,1) = S(1+2*i,1)+E(1);
    for j=2:(N-2*i)
      E(j) = E(j-1)*(2*i+j-1)/(j-1);
      S(j+2*i,1) = S(j+2*i,1)+MB(j-1)*E(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  t1 = false; % Will go true if B(k,p(2))>0
  t2 = false; % Will go true if t1 && any(B(k+1:P,p(2))<0
  
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(2)) = S(2,2)/S(1,1); if k==1 && B(1,p(2))>0, t1 = true; end
  A(1,p(2)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(2))*S(i+j,i-1)-B(i-2,p(2))*S(i+j,i-2);
    end
    B(i-1,p(2)) = S(i,i)/S(i-1,i-1);
    if t1 && B(i-1,p(2))<0; t2 = true; break;
    elseif k==i-1 && B(i-1,p(2))>0; t1 = true;
    end
    A(i-1,p(2)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  end
  if t1 && ~t2
    S(P+1,P+1) = S(P+2,P)-A(P-1,p(2))*S(P+1,P)-B(P-1,p(2))*S(P+1,P-1);
    B(P,p(2)) = S(P+1,P+1)/S(P,P);
    if B(P,p(2))<0; t2 = true; end
  end
  
  % -- Arbitrate about the value s2(p(2)) --
  if t2
    % p(2) becomes the new right bound and we jump to the next iteration
    p(7) = p(4); p(4) = p(2); p(2) = p(7); continue;
  elseif t1
    % p(2) becomes candidate to be the next left bound
    p(5) = 2;
  else
    % p(2) becomes candidate to be the next right bound
    p(6) = 2;
  end
  
  % == Second step of Ridder's method ==
  % -- Compute sigma value --
  s2(p(3)) = s2(p(2)) + (s2(p(2))-s2(p(1)))*B(k,p(2))/...
    sqrt(B(k,p(2))*B(k,p(2))-B(k,p(1))*B(k,p(4)));
  
  % -- Compute degenerated moments --
  for i=1:N-1; S(i+1,1) = MB(i); end, E(2) = 1;
  for i=1:P
    E(1) = -E(2)*s2(p(3)); S(1+2*i,1) = S(1+2*i,1)+E(1);
    for j=2:(N-2*i)
      E(j) = E(j-1)*(2*i+j-1)/(j-1);
      S(j+2*i,1) = S(j+2*i,1)+MB(j-1)*E(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  t1 = false; % Will go true if B(k,p(3))>0
  t2 = false; % Will go true if t1 && any(B(k+1:P,p(3))<0
  
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(3)) = S(2,2)/S(1,1); if k==1 && B(1,p(2))>0, t1 = true; end
  A(1,p(3)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(3))*S(i+j,i-1)-B(i-2,p(3))*S(i+j,i-2);
    end
    B(i-1,p(3)) = S(i,i)/S(i-1,i-1);
    if t1 && B(i-1,p(3))<0; t2 = true; break;
    elseif k==i-1 && B(i-1,p(3))>0; t1 = true;
    end
    A(i-1,p(3)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  end
  if t1 && ~t2
    S(P+1,P+1) = S(P+2,P)-A(P-1,p(3))*S(P+1,P)-B(P-1,p(3))*S(P+1,P-1);
    B(P,p(3)) = S(P+1,P+1)/S(P,P);
    if B(P,p(3))<0; t2 = true; end
  end
  
  % -- Arbitrate about the value s2(p(3)) --
  if ~t2 && t1 && s2(p(3))>s2(p(p(5)))
    % s2(p(3)) becomes candidate to be the next left bound
    p(5) = 3;
  end
  if ~t1 && s2(p(3))<s2(p(p(6)))
    % s2(p(3)) becomes candidate to be the next right bound
    p(6) = 3;
  end
  
  % == Apply new bounds ==
  p(7) = p(1); p(1) = p(p(5)); p(p(5)) = p(7);
  p(7) = p(4); p(4) = p(p(6)); p(p(6)) = p(7);
  
  % == Check partial convergence ==
  if nEval > nEvalMax, status(6)=true; end
  if s2(p(4))-s2(p(1))< s2ref, status(8)=true; end
end


% == FINAL SEARCH ==
while status == 0
  % -- Reset pointers to next-bound candidates --
  p(5) = 1;
  p(6) = 4;
  
  % == First step of Ridder's method ==
  % -- Compute sigma value --
  s2(p(2)) = .5*(s2(p(1))+s2(p(4)));
  
  % -- Compute degenerated moments --
  for i=1:N-1; S(i+1,1) = MB(i); end, E(2) = 1;
  for i=1:P
    E(1) = -E(2)*s2(p(2)); S(1+2*i,1) = S(1+2*i,1)+E(1);
    for j=2:(N-2*i)
      E(j) = E(j-1)*(2*i+j-1)/(j-1);
      S(j+2*i,1) = S(j+2*i,1)+MB(j-1)*E(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(2)) = S(2,2)/S(1,1);
  A(1,p(2)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(2))*S(i+j,i-1)-B(i-2,p(2))*S(i+j,i-2);
    end
    B(i-1,p(2)) = S(i,i)/S(i-1,i-1);
    A(i-1,p(2)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(2))*S(P+1,P)-B(P-1,p(2))*S(P+1,P-1);
  B(P,p(2)) = S(P+1,P+1)/S(P,P);
  
  % -- Arbitrate about the value s2(p(2)) --
  if B(P,p(2))>=0
    % p(2) becomes candidate to be the next left bound
    p(5) = 2;
  else
    % p(2) becomes candidate to be the next right bound
    p(6) = 2;
  end
  
  % == Second step of Ridder's method ==
  % -- Compute sigma value --
  s2(p(3)) = s2(p(2)) + (s2(p(2))-s2(p(1)))*B(P,p(2))/...
    sqrt(B(P,p(2))*B(P,p(2))-B(P,p(1))*B(P,p(4)));
  
  % -- Compute degenerated moments --
  for i=1:N-1; S(i+1,1) = MB(i); end, E(2) = 1;
  for i=1:P
    E(1) = -E(2)*s2(p(3)); S(1+2*i,1) = S(1+2*i,1)+E(1);
    for j=2:(N-2*i)
      E(j) = E(j-1)*(2*i+j-1)/(j-1);
      S(j+2*i,1) = S(j+2*i,1)+MB(j-1)*E(j);
    end
  end
  nEval = nEval + 1;
  
  % -- Chebyshev algorithm --
  for i=0:(N-3)
    S(i+2,2) = S(i+3,1)-MB(1)*S(i+2,1);
  end
  B(1,p(3)) = S(2,2)/S(1,1);
  A(1,p(3)) = S(3,2)/S(2,2)-S(2,1)/S(1,1);
  for i=3:P
    for j=0:(N+1-2*i)
      S(i+j,i) = S(i+j+1,i-1)-A(i-2,p(3))*S(i+j,i-1)-B(i-2,p(3))*S(i+j,i-2);
    end
    B(i-1,p(3)) = S(i,i)/S(i-1,i-1);
    A(i-1,p(3)) = S(i+1,i)/S(i,i)-S(i,i-1)/S(i-1,i-1);
  end
  S(P+1,P+1) = S(P+2,P)-A(P-1,p(3))*S(P+1,P)-B(P-1,p(3))*S(P+1,P-1);
  B(P,p(3)) = S(P+1,P+1)/S(P,P);
  
  % -- Arbitrate about the value s2(p(3)) --
  if B(P,p(3))>=0 && s2(p(3))>s2(p(p(5)))
    p(5) = 3;
  elseif B(P,p(3))<0 && s2(p(3))<s2(p(p(6)))
    p(6) = 3;
  end
  
  % == Apply new bounds ==
  p(7) = p(1); p(1) = p(p(5)); p(p(5)) = p(7);
  p(7) = p(4); p(4) = p(p(6)); p(p(6)) = p(7);
  
  % == Check convergence ==
  if B(P,p(1))<Bref(P), status(7)=true; end
  if s2(p(4))-s2(p(1))< s2ref, status(8)=true; end
  if nEval > nEvalMax, status(6)=true; end
end

% Set allMomentPreserved and trustMomentPreserved flags as all moment
% preserving quadratures always exist for Gauss EQMOM
status(9)=true;
status(10)=true;


% == QUADRATURE COMPUTATION ==
% -- Detect number of nodes --
n=P;
for i=1:P-1
  if B(i,p(1))<=Bref(i)
    B(i,p(1))=0;
    n=i; break;
  end
end

% -- Constructing Jacobi matrix --
J = zeros(n,n);
J(1,1) = MB(1);
for i=2:n
  J(i,i) = A(i-1,p(1));
  J(i,i-1) = sqrt(B(i-1,p(1)));
  J(i-1,i) = J(i,i-1);
end

% -- Computing weights and nodes --
sigma = sqrt(s2(p(1)));
if n==P
  [V,L] = eig(J,'vector');
  L=L';
  W = M0*V(1,:)'.^2;
else
  [V,l] = eig(J,'vector');
  w = M0*V(1,:).^2;
  L = [l' .5+zeros(1,P-n)];
  W = [w'; zeros(P-n,1)];
  status(3)=true;
end

end
% == CODE EXPLANATION ==
% -- Structure --
%  This codes solves for the value of sigma^2 that yields:
%    b_k(\sigma^2)>0, k\in{1,...,P-1}
%    b_P(\sigma^2)=0
%
%  The call to external functions is kept at the minimum to make the code
%  faster, by minimising the number of memory allocations and releases.
%  The only external function called is ``eig'' at the end of the
%  computation (with the exception of basic mathematical operations).
%
%  The search of the solution if splitted in three steps:
%   i.   Evaluation of a_k and b_k for s^2 = 0 and s^2 = s^2_max.
%   ii.  Search of interval [s^2_l, s^2_r] in which b_k(\sigma^2)>0
%        k\in{1,...,P-1}, b_P(s^2_l)>0 and b_P(s^2_r)<0.
%   iii. Search of the value s^2 such that b_P(s^2)=0.
%
%  The steps ii. and iii. are implementations of the Ridder's method.
%
%  In step ii., two logical flags are associated to each tested s^2 value.
%  These allow to stop the computations of the Chebyshev algorithm as soon
%  as enough information is known about the tested s^2 value to detect
%  whether it is higher or lower than the searched root.
%
%  For each tested value of s^2, we first compute the associated
%  degenerated moments. These are placed in the first column of the matrix
%  ``S'' and are computed through nested loops instead of performing the
%  whole matrix-vector product described in the documentation.
%
%  The variable ``p'' acts as a pointer. More precisely, the values p(1:4)
%  contains the column numbers in which are holds the informations about
%  tested s^2 values:
%   * columns p(1) of A, B and s2 hold information about the left bound of
%   the search interval
%   * columns p(4) of A, B and s2 hold information about the right bound
%   * columns p(2) and p(3) of A, B and s2 are used to store informations
%   of the two s^2 values tested by the Ridder's algorithm.
%
%  At the end of each Ridder's iteration, B(:,p(1)) contains only positive
%  values, and B(:,p(4)) contains at least one negative value.
%
% -- Variables --
% * A: matrix whose columns hold the a_k coefficients, k\in{2,...,P-1}, for
%      multiple s^2 values.
% * B: matrix whose columns hold the b_k coefficients, k\in{1,...,P}, for
%      multiple s^2 values.
% * s2: row vector holding the tested s^2 values.
% * S: matrix used to compute the Chebyshev algorithm
% * E: column vector used to compute efficiently the degenerated moments
% * MB: vector of reduced moments, defined by m_k/m_0.