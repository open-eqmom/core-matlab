function [W,L,sigma,status] = computeGamma2(M,opts)
% Method computing the parameters of the main quadrature of EQMOM for a
% bi-Gamma reconstruction;
% Takes a vector of 5 moments as input and formulates a 2 nodes extended
% quadrature.
%
% >> [W,L,sigma,status] = computeGamma2(M,opts)
%   *Inputs :
%      + M     : Vector of realizable moments     (5*1)[double]
%      + (opts): Option array from eqmomset       (1*8)[logical]
%   *Outputs :
%      + W     : Column vector of KDF weights     (2*1)[double,>=0]
%      + L     : Row vector of KDF locations      (1*2)[double,>=0]
%      + sigma : Scalar value of spreading factor (1*1)[double,>=0]
%      + status: Status code about convergence    (1*16)[logical]

% == SOLVER PARAMETERS ==
if nargin==1
  opts = eqmomset();
end
relTol   = 1e-10;
status   = false(1,16);

% == CHECKING INPUTS ==
if opts(1)
  if ~isequal(size(M),[5 1]) || ~isnumeric(M) || ~isreal(M) || ...
      any(isnan(M) | ~isfinite(M))
    status(1)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  end
end

% == INPUT PREPROCESSING ==
% -- Checking m0>0 --
if M(1) < 0
  % -- Unrealisable moments --
  status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
elseif M(1) == 0
  % -- Null distribution --
  status(2)=true; status(3)=true;
  W = [0; 0]; L = [.5 .5]; sigma = 0;
  if opts(2) % Checking that all moments are null
    status(10) = true;
    status(9) = ~any(M~=0);
  end
  return;
end

% -- Computing reduced moments --
iM0 = 1/M(1);
MB1 = M(2)*iM0;
MB2 = M(3)*iM0;
MB3 = M(4)*iM0;
MB4 = M(5)*iM0;

% -- Often used square values --
MB1S = MB1*MB1;
MB2S = MB2*MB2;

% == INITIAL REALIZABILITY CHECK ==
% -- Computation of Hankel determinants --
H = [MB1;
  MB2-MB1S;
  MB1*MB3-MB2S;
  0];
H(4) = MB4*H(2) - MB3*(MB3 - MB2*MB1) + MB2*(MB3*MB1-MB2S);

% -- Test of Hankel determinants values --
for i=1:4
  if H(i)<-relTol
    % -- Unrealisable moments --
    status(5)=true; W = [NaN; NaN]; L = [NaN NaN]; sigma = NaN; return;
  elseif H(i)<relTol
    % -- Degenerated moments --
    status(2)=true;
    sigma = 0;
    break;
  end
end

% == COMPUTING QUADRATURE ==
if status==0
  e = (2*MB1S*MB2 - 4*MB3*MB1 + 2*MB2S)/(MB1*MB2);
  c1 = -((2*MB1S*MB2 - 4*MB3*MB1 + 2*MB2S)^2 - 6*MB1*MB2*(- 4*MB3*MB1S + 3*MB1*MB2S + MB4*MB1))/(12*MB1S*MB2S);
  c2 = ((MB1S*MB2 + MB3*MB1 - 2*MB2S)*(4*MB1S*MB1S*MB2S + 8*MB1S*MB1*MB2*MB3 - 7*MB1S*MB2S*MB2 + 18*MB4*MB1S*MB2 - 32*MB1S*MB3*MB3 + 11*MB1*MB2S*MB3 - 2*MB2S*MB2S))/(54*MB1S*MB1*MB2S*MB2);
  Delta = 4*c1*c1*c1+27*c2*c2;
  if abs(Delta)<relTol
    tr = 0;
  elseif Delta>0
    if 4*c1*c1*c1>relTol*27*c2*c2
      d = .5*(sqrt(27*Delta)-27*c2);
      if d>0
        d = d^(1/3);
      else
        d = -(-d)^(1/3);
      end
    else
      d = 0;
    end
    if abs(d)>relTol
      tr = d/3-c1/d;
    else
      tr = -sign(c2)*abs(c2)^(1/3);
    end
  else
    d = sqrt(-c1/3);
    if d>relTol
      tr = 2*d*cos((acos(1.5*c2/(d*c1))-4*pi)/3);
    else
      tr=0;
    end
  end
  sigma = tr - e/6;
  status(8)=true;
end

e1 = MB2-MB1S-MB1*sigma;
e2 = MB1*sigma*sigma+3*(MB1S-MB2)*sigma+MB3-3*MB2*MB1+2*MB1S*MB1;
if abs(e2) < relTol || (4*e1*e1*e1<relTol*e2*e2)
  W = [M(1); 0];
  L = [MB1 .5];
  status(3)=true;
else
  e3 = e2/sqrt(e2*e2+4*e1*e1*e1);
  if (1+e3)*MB1S+e1*(e3-1)>relTol
    W = .5*M(1)*[1+e3; 1-e3];
    L = MB1+[-sqrt(e1*(1-e3)/(1+e3)) sqrt(e1*(1+e3)/(1-e3))];
    status(7)=false; status(9)=true; status(10)=true;
  else
    % == Compute quadrature with L(1)=0 ==
    sigma = MB3/MB2 - MB2/MB1;
    L = [0 2*MB2/MB1-MB3/MB2];
    W2 = MB1/L(2);
    W = M(1)*[1-W2; W2];
    status(4)=true; status(7)=false; status(9)=false; status(10)=true;
  end
end
end