function v = evaluateBetaEqmom(W,L,s,x)
% Method evaluating point-wise values of a Beta-EQMOM approximation.
%
% v = sum_{i=1}^P W(i) * K(x,L(i),s)
% with
% K(x,L,s) = x^(l-1)*(1-x)^(m-1)/Beta(l,m), l=L/s, m=(1-L)/s
%
% >> v = evaluateBetaEqmom(W,L,s,x)
%   *Inputs :
%      + W     : Row vector of KDF weights        (1*P)[double,>=0]
%      + L     : Column vector of KDF locations   (P*1)[double,>0,<1]
%      + s     : Scalar value of spreading factor (1*1)[double,>0]
%      + x     : Vector of evaluation locations   (N*1)[double,>0,<1]
%   *Outputs :
%      + v     : Vector of NDF evaluations        (N*1)[double,real]

% == CHECKING INPUTS ==
P = length(W);

validateattributes(W,{'double'},{'column','finite','nonnegative'});
validateattributes(L,{'double'},{'row','finite','numel',P,'positive','<',1});
validateattributes(s,{'double'},{'scalar','finite','positive'});
validateattributes(x,{'double'},{'column','finite','positive','<',1});

% == COMPUTING VALUES OF APPROXIMATION ==
l = L/s;
m = (1-L)/s;
v = bsxfun(@power,x,l-1).*bsxfun(@power,1'-x,m-1)*(W./beta(l,m)');
end