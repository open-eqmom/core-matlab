function v = evaluateLogNormalEqmom(W,L,s,x)
% Method evaluating point-wise values of a LogN-EQMOM approximation.
%
% v = sum_{i=1}^P W(i) * K(x,L(i),s)
% with
% K(x,L,s) = exp(-(log(x)-log(L))^2/(2*s^2))/(s*L*sqrt(2*pi))
%
% >> v = evaluateLogNormalEqmom(W,L,s,x)
%   *Inputs :
%      + W     : Row vector of KDF weights        (P*1)[double,>=0]
%      + L     : Column vector of KDF locations   (1*P)[double,>0]
%      + s     : Scalar value of spreading factor (1*1)[double,>0]
%      + x     : Vector of evaluation locations   (N*1)[double,>0]
%   *Outputs :
%      + v     : Vector of NDF evaluations        (N*1)[double,>=0]

% == CHECKING INPUTS ==
P = length(W);

validateattributes(W,{'double'},{'column','finite','nonnegative'});
validateattributes(L,{'double'},{'row','finite','numel',P,'positive'});
validateattributes(s,{'double'},{'scalar','finite','positive'});
validateattributes(x,{'double'},{'column','finite','positive'});

% == COMPUTING VALUES OF APPROXIMATION ==
v = exp(-bsxfun(@minus,log(x),log(L)).^2/(2*s*s))*(W./(s*L'*sqrt(2*pi)));
end