function v = evaluateLaplaceEqmom(W,L,s,x)
% Method evaluating point-wise values of a Laplace-EQMOM approximation.
%
% v = sum_{i=1}^P W(i) * K(x,L(i),s)
% with
% K(x,L,s) = exp(-abs(x-L)/s)/(2*s);
%
% >> v = evaluateLaplaceEqmom(W,L,s,x)
%   *Inputs :
%      + W     : Row vector of KDF weights        (P*1)[double,>=0]
%      + L     : Column vector of KDF locations   (1*P)[double,real]
%      + s     : Scalar value of spreading factor (1*1)[double,>0]
%      + x     : Vector of evaluation locations   (N*1)[double,real]
%   *Outputs :
%      + v     : Vector of NDF evaluations        (N*1)[double,>=0]

% == CHECKING INPUTS ==
P = length(W);

validateattributes(W,{'double'},{'column','finite','nonnegative'});
validateattributes(L,{'double'},{'row','finite','numel',P});
validateattributes(s,{'double'},{'scalar','finite','positive'});
validateattributes(x,{'double'},{'column','finite'});

% == COMPUTING VALUES OF APPROXIMATION ==
v = exp(-abs(bsxfun(@minus,x,L))/s)/(2*s)*W;
end