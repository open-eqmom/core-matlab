function v = evaluateGammaEqmom(W,L,s,x)
% Method evaluating point-wise values of a Gamma-EQMOM approximation.
%
% v = sum_{i=1}^P W(i) * K(x,L(i),s)
% with
% K(x,L,s) = x.^(L/s-1)*exp(-x/s)/(gamma(L/s)*s^(L/s))
%
% >> v = evaluateGammaEqmom(W,L,s,x)
%   *Inputs :
%      + W     : Row vector of KDF weights        (1*P)[double,>=0]
%      + L     : Column vector of KDF locations   (P*1)[double,>0]
%      + s     : Scalar value of spreading factor (1*1)[double,>0]
%      + x     : Vector of evaluation locations   (N*1)[double,>0]
%   *Outputs :
%      + v     : Vector of NDF evaluations        (N*1)[double,real]

% == CHECKING INPUTS ==
P = length(W);

validateattributes(W,{'double'},{'column','finite','nonnegative'});
validateattributes(L,{'double'},{'row','finite','numel',P,'positive'});
validateattributes(s,{'double'},{'scalar','finite','positive'});
validateattributes(x,{'double'},{'column','finite','positive'});

% == COMPUTING VALUES OF APPROXIMATION ==
l = L/s;
v = bsxfun(@power,x,l)*(W./(gamma(l).*s.^l)') .* exp(-x/s);
end