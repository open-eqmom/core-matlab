function v = evaluateWeibullEqmom(W,L,s,x)
% Method evaluating point-wise values of a Weibull-EQMOM approximation.
%
% v = sum_{i=1}^P W(i) * K(x,L(i),s)
% with
% K(x,L,s) = exp(-l^(1/s))*l.^((1-s)/s)/(s*L), l=x/L
%
% >> v = evaluateWeibullEqmom(W,L,s,x)
%   *Inputs :
%      + W     : Row vector of KDF weights        (P*1)[double,>=0]
%      + L     : Column vector of KDF locations   (1*P)[double,>0]
%      + s     : Scalar value of spreading factor (1*1)[double,>0]
%      + x     : Vector of evaluation locations   (N*1)[double,>0]
%   *Outputs :
%      + v     : Vector of NDF evaluations        (N*1)[double,>=0]

% == CHECKING INPUTS ==
P = length(W);

validateattributes(W,{'double'},{'column','finite','nonnegative'});
validateattributes(L,{'double'},{'row','finite','numel',P,'positive'});
validateattributes(s,{'double'},{'scalar','finite','positive'});
validateattributes(x,{'double'},{'column','finite','positive'});

% == COMPUTING VALUES OF APPROXIMATION ==
t = bsxfun(@ldivide,x,L);
v = t.^((1-s)/s).*exp(-t.^(1/s))*(W./(s*L'));
end