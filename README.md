# README

## Generalities

This repository contains MATLAB implementations of the Extended Quadrature Method Of Moments (EQMOM).
It is splitted in the following folders:

* [mainQuadrature](mainQuadrature): functions implementing the core moment-inversion procedure of EQMOM. Each function corresponds to a specific kernel and range of method order.
* [nestedQuadrature](nestedQuadrature): functions computing Gauss-quadrature rules used in EQMOM for nested quadratures.
* [ndfEvaluation](ndfEvaluation): functions computing point-wise values of a EQMOM approximation.

## License

All source codes in this repository are released under MIT License.
See [this file](LICENSE) for supplementary details.

## Contributing

All contributions are welcome do not hesitate to submit merge request.
In particular, if a solver does not behave as expected, please fill up an issue with

*  Name of used solver;
*  Moment set on which the EQMOM moment inversion is applied;
*  Options used (if any non-default);
*  Matlab version;
*  Short description of erroneous behaviour (run-time execution error? Out-of expected range results? Innacurate status code? ...).

If you use a modified version of source codes, please attach them along with your issue.